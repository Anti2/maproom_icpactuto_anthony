To build maproom you need the following software:

	* Java 1.6.0_10+
	* GNU Make 3.81+
	* Unix shell
	* Raptor 1.4.18+ or 2.0.15 (for El Capitan)
	* semantic_tools 1.3.2+
	* miconf 1.7.1+ (https://bitbucket.org/iridl/miconf/downloads)
    * Python with ply modeule (sudo easy_install ply)
    * Node.js with jade and html2jade module (sudo npm -g install jade html2jade)

To build and set up:

	* Edit config.lua
	* Run './build.sh tarball' or 'make tarball'
	* Drop localmaproom.conf into your Apache config dir


To convert a page into jade you may use html2jade as follows:

	html2jade --noemptypipe <foo.html >foo.jade.m
	html2jade --noemptypipe <boo.xhtml >boo.xjade.m


	